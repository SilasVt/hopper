# hopper-cli

hopper is a command line application that makes moving to a new Linux installation. hopper is able to scan your system for installed apps, save your installed apps to a file and install your apps on another installation. It is in development and non functional.

## Installation

Just Download the executable file for hopper once it get released and save it anywhere. Simply run it 

```bash
./hopper
```
## First Steps
After Running hopper for the first time it will check for specific directories/files and create them if necessary.
In the main menu press 0 to enter configuration-mode. In configuration mode you can select one of the 
packaging systems and select if hopper should install apps with, or save apps from that packaging system.

## Usage
1.  Run hopper, configure if you haven't done so (first steps).

2. With hopper configured, in the main menu select "Save apps to file". Hopper will now scan your system for apps installed with the chosen packaging systems.

3. After saving your apps with hopper you should create a backup of your HOME directory. Later move the HOME backup to your new Linux install. If you do not want to keep your HOME directory then only create a backup of ~/Applications/hopper and place it in the HOME of your new install.

4. On your new installation launch hopper. If all files are in the correct place hopper will launch straight into the main menu. From the main menu tweak your configuration if desired. Or simply install the apps with the option "Install from file".

## Roadmap
The Roadmap is divided into different sections. There are no time frames for any of these.

Section 0 (Current): Getting the base of hopper working for Snaps and Flatpaks.

Section 1: Getting native packages for apt, DNF and Zypper working.

Section 2: Getting native packages working without any issues of unwanted packages and unwanted side effects.

Section 3: Slowly add support for other package managers.
