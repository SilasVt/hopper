#include "globals.h"
#include <iostream>
#include "sysInteraction.h"
#include <filesystem>
#include <fstream>
#include <vector>
#include <unistd.h>

bool checkFolderFiles(const std::string path){
if (std::filesystem::exists(path)) return true;
else return false;

}

void reCheckDir(void){

if (!checkFolderFiles(appsdir) || !checkFolderFiles(hopperdir) || !checkFolderFiles(hopperConfigDir) || !checkFolderFiles(snapdir) /* || !checkFolderFiles(flatpakdir)*/) {
std::cout << "Error: Files or Directories have been changed outside of and during the operation of hopper! \nPlease Restore Files/Directories manually or restart hopper.\nhopper will exit!";
exit(1);
}
}

void loadConfigsFromFile(void){
reCheckDir();
std::cout << "Loading config from file!" << std::endl;
std::ifstream file(hopperConfigDir);
std::string line;
unsigned int lineNumber = 0;

while(std::getline(file, line)){
lineNumber++;
//std::cout << lineNumber << " " << line << "line length: " << line.length() << std::endl;
//load flatpak config
if( (lineNumber == 4) & (line == "0")) Flatpak.installWith = false;
if( (lineNumber == 4) & (line == "1")) Flatpak.installWith = true;
if( (lineNumber == 6) & (line == "0")) Flatpak.searchFor = false;
if( (lineNumber == 6) & (line == "1")) Flatpak.searchFor = true;

//load dnf config
if( (lineNumber == 9) & (line == "0")) DNF.installWith = false;
if( (lineNumber == 9) & (line == "1")) DNF.installWith = true;
if( (lineNumber == 11) & (line == "0")) DNF.searchFor = false;
if( (lineNumber == 11) & (line == "1")) DNF.searchFor = true;

//load zypper config
if( (lineNumber == 14) & (line == "0")) Zypper.installWith = false;
if( (lineNumber == 14) & (line == "1")) Zypper.installWith = true;
if( (lineNumber == 6) & (line == "0")) Zypper.searchFor = false;
if( (lineNumber == 6) & (line == "1")) Zypper.searchFor = true;

//load Snap config
if( (lineNumber == 19) & (line == "0")) Snap.installWith = false;
if( (lineNumber == 19) & (line == "1")) Snap.installWith = true;
if( (lineNumber == 21) & (line == "0")) Snap.searchFor = false;
if( (lineNumber == 21) & (line == "1")) Snap.searchFor = true;
}

}









void checkDir(void){

if (!checkFolderFiles(appsdir)){
std::cout << "----DIR ERROR----\n";
bool created = false;
while(!created){
std::string createQuestion;
std::cout << "~/Applications is not a directory.\n";
std::cout << "This Directory is where all .AppImage Apps should be saved.\n";
std::cout << "~/Applications is also where hopper stores files!\n";
std::cout << "Automatically create ~/Applications ? yes or no\n";
std::cin >> createQuestion;
std::cout << "You entered: " << createQuestion << std::endl;
std::cin.clear ();
std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
    if(createQuestion == "no") exit(0);

    if(createQuestion == "yes") {
        execute("mkdir ~/Applications");
        if (checkFolderFiles(appsdir))
        {std::cout << "Created ~/Applications\n";
        created = true;
        }
        else exit(1);
        }
}
}//End of ~/Applications


if (!checkFolderFiles(hopperdir)){
std::cout << "----DIR ERROR----\n";
bool created2 = false;
std::cout << "~/Applications/hopper is not a directory.\n";
std::cout << "~/Applications/hopper is where hopper stores files!\n";
std::cout << "Automatically create ~/Applications/hopper ? yes or no\n";
std::string createQuestion;

while(!created2){
std::cin >> createQuestion;
std::cout << "You entered: " << createQuestion << std::endl;
std::cin.clear ();
std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
    if(createQuestion == "no") exit(0);

    if(createQuestion == "yes") {
        execute("mkdir ~/Applications/hopper");
        if (checkFolderFiles(hopperdir))
        {std::cout << "Created ~/Applications/hopper\n";
        created2 = true;
        }
        else exit(1);
        }

}
}

if (!checkFolderFiles(hopperConfigDir)){
std::cout << "----FILE ERROR----\n";
bool created3 = false;
std::cout << "~/Applications/hopper/hopper_config.txt is not a file.\n";
std::cout << "This file stores your configuration/settings for hopper\n";
std::cout << "Automatically create file? yes or no\n";
std::string createQuestion;

while(!created3){
std::cin >> createQuestion;
std::cout << "You entered: " << createQuestion << std::endl;
std::cin.clear ();
std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
    if(createQuestion == "no") exit(0);

    if(createQuestion == "yes") {
        execute("touch ~/Applications/hopper/hopper_config.txt");
        if (checkFolderFiles(hopperConfigDir))
        {
        std::ofstream outfile (hopperConfigDir);
        outfile <<
"hopper_config.txt\n\n#Install Using Flatpak\n0\n#Save from Flatpak\n0\n\n#Install Using DNF\n0\n#Save from DNF\n0\n\n#Install Using Zypper\n0\n#Save from Zypper\n0\n\n#Install Using Snap\n0\n#Save from Snap\n0\n" << std::endl;

outfile.close();

        std::cout << "Created ~/Applications/hopper/hopper_config.txt\n";
        created3 = true;
        }
        else exit(1);
        }

}
}


if (!checkFolderFiles(snapdir)){
std::cout << "----FILE ERROR----\n";
bool created4 = false;
std::cout << "~/Applications/hopper/snap_apps.txt is not a file.\n";
std::cout << "This file is used to store snap apps!\n";
std::cout << "Automatically create file? yes or no\n";
std::string create2Question;

while(!created4){
std::cin >> create2Question;
std::cout << "You entered: " << create2Question << std::endl;
std::cin.clear ();
std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
    if(create2Question == "no") exit(0);

    if(create2Question == "yes") {
        execute("touch ~/Applications/hopper/snap_apps.txt");
        if (checkFolderFiles(snapdir))
        {
        std::ofstream outfile2 (snapdir);
        outfile2 << "snap_apps.txt Not yet written to\n" << std::endl; outfile2.close();

        std::cout << "Created ~/Applications/hopper/snap_apps.txt\n";
        created4 = true;
        }
        else exit(1);
        }

}
}

if (!checkFolderFiles(flatpakdir)){
std::cout << "----FILE ERROR----\n";
bool created5 = false;
std::cout << "~/Applications/hopper/flatpak_apps.txt is not a file.\n";
std::cout << "This file is used to store flatpaks!\n";
std::cout << "Automatically create file? yes or no\n";
std::string create3Question;

while(!created5){
std::cin >> create3Question;
std::cout << "You entered: " << create3Question << std::endl;
std::cin.clear ();
std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered
    if(create3Question == "no") exit(0);

    if(create3Question == "yes") {
        execute("touch ~/Applications/hopper/flatpak_apps.txt");
        if (checkFolderFiles(flatpakdir))
        {
        std::ofstream outfile3 (flatpakdir);
        outfile3 << "flatpak_apps.txt Not yet written to\n" << std::endl; outfile3.close();

        std::cout << "Created ~/Applications/hopper/flatpak_apps.txt\n";
        created5 = true;
        }
        else exit(1);
        }

}
}



std::cout << R"(------------------------------------)" << std::endl;
} //end of Check Dirs


void createPaths(void){


homedir = getenv("HOME");

appsdir.append(homedir);
appsdir.append("/Applications");

hopperdir = appsdir;
hopperdir.append("/hopper");

snapInstallDir = homedir;
snapInstallDir.append("/snap");

snapdir = hopperdir;
snapdir.append("/snap_apps.txt");

hopperConfigDir = hopperdir;
hopperConfigDir.append("/hopper_config.txt");

flatpakdir.append(hopperdir);
flatpakdir.append("/flatpak_apps.txt");

Flatpak.managerName = "Flatpak";
DNF.managerName = "DNF";
Zypper.managerName = "Zypper";
Snap.managerName = "Snap";
apt.managerName = "apt";
}

void saveConfigToFile(void){
std::string tempConfigFile;

for (int i = 1; i < 22; i++){

if(i==1) tempConfigFile.append("hopper_config.txt\n\n#Install Using Flatpak\n"); //write lines 1...3

if((i==4)&&(Flatpak.installWith == 1)) tempConfigFile.append("1\n#Save from Flatpak\n"); //write lines 4 and 5 for positive flatpak value
if((i==4)&&(Flatpak.installWith == 0)) tempConfigFile.append("0\n#Save from Flatpak\n"); //write lines 4 and 5 for negative flatpak value
if((i==5)&&(Flatpak.searchFor == 1)) tempConfigFile.append("1\n\n#Install Using DNF\n"); //write lines 6..8 for positive flatpak value
if((i==5)&&(Flatpak.searchFor == 0)) tempConfigFile.append("0\n\n#Install Using DNF\n"); //write lines 6..8 for positive flatpak value

if(i==6) tempConfigFile.append("0\n#Save from DNF\n0\n\n#Install Using Zypper\n0\n#Save from Zypper\n0\n\n#Install Using Snap\n"); //Always write DNF and Zypper as zero because its not implemented. also i forgot apt anyways

if((i==19)&&(Snap.installWith == 1)) tempConfigFile.append("1\n#Save from Snap\n"); //write lines 19 and 20 for positive flatpak value
if((i==19)&&(Snap.installWith == 0)) tempConfigFile.append("0\n#Save from Snap\n"); //write lines 19 and 20 for negative flatpak value
if((i==21)&&(Snap.searchFor == 1)) tempConfigFile.append("1\n"); //write lines 21 for positive flatpak value
if((i==21)&&(Snap.searchFor == 0)) tempConfigFile.append("0\n"); //write lines 21 for positive flatpak value

}
reCheckDir();
std::ofstream outfile (hopperConfigDir);
        outfile << tempConfigFile;
        outfile.close();

        std::cout << "Saved config to ~/Applications/hopper/hopper_config.txt\n";

}
