#include <iostream>
#include "sysInteraction.h"
#include "globals.h"
#include "filemanagment.h"

int mainMenu(void){
reCheckDir();
using namespace std;


int selection = -1;
bool selected = false;

while(!selected){
cout << "----MAIN MENU----\n";
cout << "Select Actions:\n";
cout << "Enter 0 to change configuration \nEnter 1 to save applications to file\nEnter 2 to load and install applications from file\nPress Ctrl+C at any time to exit!\n";


cin >> selection;
cout << "You entered: " << selection << endl;

cin.clear ();
cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

if(selection == SAVEMODE || selection == LOADMODE || selection == CONFIGUREMODE){
selected = true;}

}//End of input loop

reCheckDir();
return selection;
}
