#include <iostream>
#include <fstream>
#include <climits>
#include <filesystem>
#include <sys/stat.h>
#include "introCo.h"
#include "globals.h"
#include "sysInteraction.h"
#include "filemanagment.h"
#include "config.h"
#include "menu.h"

void installApps(void){
reCheckDir();
system("clear");
std::cout << "--LOADING  APPS--\n";
if((Flatpak.installWith == false) && (Snap.installWith == false) /*&& (DNF.installWith == false)&& (apt.installWith == false) && (Zypper.installWith == false)*/) { std::cout << "No package manager was selected: Please run configuration first!\n"; return;}

else{
bool selecting = true;
std::string selection;
while(selecting){
    std::cout << "Loading and Installing Apps using: ";
    if(Flatpak.installWith == true) std::cout << "Flatpak ";
    if(Snap.installWith == true) std::cout << "Snap ";
std::cout << "\nIs this correct? (yes/no): ";

    std::cin >> selection;
    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(selection == "no") break;
    if(selection == "yes"){

    std::cout << "testing the install";

    selecting = false;}
}
}
}//end installApps


void saveApps(void){
reCheckDir();
system("clear");
std::cout << "---SAVING APPS---\n";

if((Flatpak.searchFor == false) && (Snap.searchFor == false) /*&& (DNF.searchFor == false)&& (apt.searchFor == false) && (Zypper.searchFor == false)*/) { std::cout << "No package manager was selected: Please run configuration first!\n"; return;}
else{
bool selecting = true;
std::string selection;
while(selecting){
    std::cout << "Saving currently installed apps from: ";
    if(Flatpak.searchFor == true) std::cout << "Flatpak ";
    if(Snap.searchFor == true) std::cout << "Snap ";
std::cout << "\nIs this correct? (yes/no): ";

    std::cin >> selection;
    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(selection == "no") break;

    if(selection == "yes"){
    saveSnap(Snap.searchFor);

    selecting = false;}
}
}
}




int main() {

checkUser();
createPaths();
checkDir();
loadConfigsFromFile();


//check for package managers and give internal ID
if (checkManagers() == 0) { std::cout << "No compatible packaging systems found."; return 1;}

setManagerID();

saveSnap(true);


//Text intro
intro();


while(1){

//Open mainMenu and run selection
int selection = mainMenu();
if(selection == CONFIGUREMODE) configurator();
if(selection == LOADMODE) installApps();
else if(selection == SAVEMODE) saveApps();

}


return 0;
}


