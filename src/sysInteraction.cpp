#include<globals.h>
#include <filesystem>
#include <sys/stat.h>
#include "filemanagment.h"
#include <iostream>
#include <fstream>
//run and save output of command
std::string execute(std::string command) {
    using namespace std;
   char buffer[10000];
   string result = "";

   // Open pipe to file
   FILE* pipe = popen(command.c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);

   return result;
}

//Check for packagemanager; managerIn = command like flatpak --version; checkFor = first 4 expected letters
bool check(std::string managerIn, std::string checkFor) {
    reCheckDir();
    int checkNum = 0;
    std::string manager = execute(managerIn);
    for(unsigned short int i = 0; i != 4; i++) {

    if( manager[i] == checkFor[i])
    {checkNum++;}
    }
    if(checkNum == 4) {return true;}
    else {return false;}
    reCheckDir();
}














void saveSnap(const bool& save){
if(save){
std::string snapCommand = "ls ";
snapCommand.append(snapInstallDir);
std::string tempStorage = "snap install ";
std::string tempStorage2;
    std::string snapResult = execute(snapCommand);

    for(unsigned short int i = 0; i <= snapResult.length(); i++){
    if(snapResult[i] == '\n') tempStorage.append(" && snap install ");
    else {tempStorage2 = snapResult[i]; tempStorage.append(tempStorage2);}
    }

    tempStorage.erase(tempStorage.size()-17);

        std::ofstream outfile (snapdir);
        outfile << tempStorage;
        outfile.close();

        std::cout << "Saved the found Snap packages to ~/Applications/hopper/snap_apps.txt\n";

}
}

















void setManagerID(void){
short int managerIDtemp = 1;

for(int i = 0; i != 5; i++){

        if( i == 1 ){
        if(Snap.isAvailable == true) {
            Snap.managerID = managerIDtemp;

            ++managerIDtemp;
            }
        }

        if( i == 0 ){
        if(Flatpak.isAvailable == true){

            Flatpak.managerID = managerIDtemp;

            managerIDtemp++;
            }
        }

        if( i == 2 ){
        if(DNF.isAvailable == true){

            DNF.managerID = managerIDtemp;
            managerIDtemp++;
            }
        }

        if( i == 3 ){
        if(apt.isAvailable == true){

            apt.managerID = managerIDtemp;
            managerIDtemp++;
            }
        }

        if( i == 4 ){
        if(Zypper.isAvailable == true){

            Zypper.managerID = managerIDtemp;
            managerIDtemp++;
            }
        }
}
}

int checkManagers (void) {
    reCheckDir();
	std::cout << R"(------------------------------------)" << '\n';
    short int managerNumber = 0;
    std::cout << "Searching for available package managers:\n";

    if (check("flatpak --version", "Flat")) {
    Flatpak.isAvailable = true;
    std::cout << "Found Flatpak! \n";
    managerNumber++;
    }

    if (check("apt --version", "apt ")) {
    apt.isAvailable = true;
    std::cout << "Found apt! \n";
    managerNumber++;
    }

    if (check("snap version", "snap")) {
    Snap.isAvailable = true;
    std::cout << "Found Snap! \n";
    managerNumber++;
    }

    if (check("zypper --version", "zypp")) {
    Snap.isAvailable = true;
    std::cout << "Found Zypper! \n";
    managerNumber++;
    }

    if (check("dnf -c", "usag")) {
    Snap.isAvailable = true;
    std::cout << "Found DNF! \n";
    managerNumber++;
    }


    if(managerNumber > 0){
    std::cout << "Search Complete!\n";
    std::cout << managerNumber << " Packaging systems found!\n";
    }


    else

    {
    std::cout << "Search Failed!\n";
    std::cout << managerNumber << " Supported packaging systems found!\n";
    }
    std::cout << R"(------------------------------------)" << std::endl;



    return managerNumber;


}


void checkUser(void){
std::string tempHome = getenv("HOME");
std::cout << tempHome << std::endl;
if (tempHome == "/root") {
std::cout << "hopper has encountered an error: hopper is being run by the user root and can't function correctly.\nIf you ran hopper via sudo try launching with sudo -E\nhopper will exit" << std::endl;
exit(1);
}

}
