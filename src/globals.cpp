#include "globals.h"

struct scanInstall Flatpak;
struct scanInstall DNF;
struct scanInstall Zypper;
struct scanInstall Snap;
struct scanInstall apt;



std::string homedir;
std::string appsdir;
std::string hopperdir;
std::string snapdir;
std::string flatpakdir;
std::string hopperConfigDir;
std::string hopperSaveDir;
std::string snapInstallDir;
std::string flatpakInstallDir;

const short int CONFIGUREMODE = 0;
const short int SAVEMODE = 1;
const short int LOADMODE = 2;

const std::string VERSION = "v0.0";
