#include <iostream>
#include "filemanagment.h"
#include "globals.h"
#include <fstream>

void showConfiguration(const scanInstall &manager){
reCheckDir();
system("clear");
std::cout << "Current Configuration for " << manager.managerName << ":\n";

std::cout << "Save Apps Currently Installed to File: ";
if(manager.searchFor) std::cout << "yes\n";
else std::cout << "no \n";

std::cout << "Load From File and Install Apps : ";
if(manager.installWith) std::cout << "yes\n";
else std::cout << "no \n";

}

void loadConfigs(void){
std::ifstream infile;
infile.open(hopperConfigDir);


}

void changeConfiguration(scanInstall &manager){
bool ask = true;
std::string selection;
int selection2;

while(ask){
reCheckDir();
std::cout << "Do you want to change configuration of " << manager.managerName << "? (yes/no)\n";

std::cin >> selection;
std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(selection == "yes" || selection == "no"){
    ask = false;}
}
if(selection == "yes"){
bool configuring = true;

while(configuring){
std::cout << "Which option should be changed?\n";
std::cout << "Save " << manager.managerName << " apps currently installed to a file (1) \nLoad from file and install apps with " << manager.managerName <<" (2) \nOr enter 0 to end configuration of " << manager.managerName << std::endl;
    std::cin >> selection2;
    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(selection2 == 0) break;
    if(selection2 == 1 || selection2 == 2){
    configuring = false;}

    if(selection2 == 1){
    std::cout << "Save via " << manager.managerName << " installed apps to file? (yes/no) \n";
    bool yesNo = true;
    std::string input;

    while(yesNo){
    std::cin >> input;
    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(input == "no" || input == "yes"){
    yesNo = false;}
    }

    if(input == "yes") manager.searchFor = true;
    if(input == "no") manager.searchFor = false;
    }//end of save option

     if(selection2 == 2){
    std::cout << "Load apps from file and install with " << manager.managerName << " ? (yes/no) \n";
    bool yesNo2 = true;
    std::string input2;

    while(yesNo2){
    std::cin >> input2;
    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(input2 == "no" || input2 == "yes"){
    yesNo2 = false;}
    }

    if(input2 == "yes") manager.installWith = true;
    if(input2 == "no") manager.installWith = false;
    }//end of load option
    showConfiguration(manager);
    configuring = true;

}//end of configuring
}
system("clear");
}

void configurator(void){
reCheckDir();
system("clear");
std::cout << "-CONFIGURATIONS-\n";

configureStart:

bool configure = false;
int selection = -1;


while (!configure){
std::cout << "Only configurations for Flatpak and Snap will be saved in this version!\n";
std::cout << "Choose which of the package managers to configure: ";

    if (Flatpak.managerID != 0) std::cout << "Flatpak(" << Flatpak.managerID << "); ";
    if (Snap.managerID != 0) std::cout << "Snap(" << Snap.managerID << "); ";
    if (DNF.managerID != 0) std::cout << "DNF(" << DNF.managerID << "); ";
    if (apt.managerID != 0) std::cout << "apt(" << apt.managerID << "); ";
    if (Zypper.managerID != 0) std::cout << "Zypper(" << Zypper.managerID << "); ";
    std::cout << "\n";
    std::cout << "Or enter 0 to cancel:";

    std::cin >> selection;
    std::cout << "You entered: " << selection << std::endl;

    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(selection == 0 || ((selection == Flatpak.managerID) && Flatpak.isAvailable ) || ((selection == Snap.managerID) && Snap.isAvailable ) || ((selection == DNF.managerID) && DNF.isAvailable ) || ((selection == apt.managerID) && apt.isAvailable ) || ((selection == Zypper.managerID) && Zypper.isAvailable )  ){
    configure = true;}
    reCheckDir();
}//End of input loop

    if(selection != 0){
    if ((selection == DNF.managerID) && DNF.isAvailable ) {
    showConfiguration(DNF);
    changeConfiguration(DNF);
    }

    if ((selection == apt.managerID) && apt.isAvailable ) {
    showConfiguration(apt);
    changeConfiguration(apt);
    }

    if ((selection == Zypper.managerID) && Zypper.isAvailable ) {
    showConfiguration(Zypper);
    changeConfiguration(Zypper);
    }

    if ((selection == Flatpak.managerID) && Flatpak.isAvailable ) {
    showConfiguration(Flatpak);
    changeConfiguration(Flatpak);
    }

    if ((selection == Snap.managerID) && Snap.isAvailable ) {
    showConfiguration(Snap);
    changeConfiguration(Snap);
    }

    reCheckDir();
    std::cout << "Configure another manager? (yes/no): ";
    std::string again;
    bool againQuestion = true;

    while(againQuestion){

    std::cin >> again;
    std::cout << "You entered: " << again << std::endl;

    std::cin.clear ();
    std::cin.ignore ( 100 , '\n' );    // Get rid of any garbage that user might have entered

    if(again == "yes" || again == "no" )   againQuestion = false;
    }
    if(again == "yes") goto configureStart;
    saveConfigToFile();
    }//skip if 0 to exit
    }//End Configure
    
