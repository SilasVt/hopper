#pragma once

#include <string>

struct scanInstall {bool isAvailable; bool searchFor; bool installWith; short int managerID; std::string managerName;};

extern struct scanInstall Flatpak;
extern struct scanInstall DNF;
extern struct scanInstall Zypper;
extern struct scanInstall Snap;
extern struct scanInstall apt;


extern std::string homedir;
extern std::string appsdir;
extern std::string hopperdir;
extern std::string snapdir;
extern std::string flatpakdir;
extern std::string hopperConfigDir;
extern std::string hopperSaveDir;
extern std::string snapInstallDir;
extern std::string flatpakInstallDir;

extern const std::string VERSION;

extern const short int CONFIGUREMODE;
extern const short int SAVEMODE;
extern const short int LOADMODE;
