#pragma once

void checkDir(void);
void createPaths(void);
void reCheckDir(void);
bool checkFolderFiles(const std::string path);
void loadConfigsFromFile(void);
void saveConfigToFile(void);
